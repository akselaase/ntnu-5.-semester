# Kreyszig index

## Kildereferanse

`by [Kreyszig, p. 534] we get ...`

## Derviasjons- og intergrasjonsregler

Heeelt foran rett etter coveret

## Laplace transforms

### Tabell over formler, p. 264

### Tabell over transformasjoner, p. 265-267

### Annet:

- Definisjon p. 221
- Eksempler (`exp`, `sinh`, `cos`) p. 222-223
- Tabell over grunnleggende transformasjoner p. 224
- `s-shifting`, p. 224-225
- Transformere derivert, p. 228
- Transformere integrert, p. 229
- Initial value problem eksempel, p. 230-231
- Heaviside step, p. 234
- `t-shifting`, p. 235
- Dirac Delta, p. 242
- Partial fractions, p. 245
- Convolutions, p. 248
- Deriverte: `F'(s)`, p. 254
- Integrerte: `∫F(s)`, p. 255

## Fourier series, integrals, and transforms

- Periodisitet, p. 478

### Fourier series, p. 479

- Eksempel (square wave), p. 480-481
- Euler Formulas, p. 482
- Convergence, p. 484-485
- Euler formulas for any period `p=2L`, p. 487
- Example (rectangular wave), p. 488

### Even and odd half-range expansions, p. 490

- Fourier cosine and sine series, p. 491
- Example (triangle on `(0, L)`), p. 494-495

### Complex fourier series, p. 496-497

- Example (`e^x`), p. 498
- Parseval's identity, p. 504

### Fourier integral, p. 508

- Fourier cosine and sine integrals, p. 511
- Laplace integrals, p. 511-512

### Cosine and sine transforms, p. 514

- Examples, p. 515
- Transforms of derivatives, p. 516
- Example `f(x)=exp(-ax)`, p. 517

### Complex fourier transform, p. 518-519

- Transform of derivatives, p. 522
- Convolution theorem, p. 523

### Tables of Transforms, p. 529-531

#### Cosine transforms, p. 529

#### Sine transforms, p. 530

#### Complex transforms, p. 531

## PDEs, p. 535

- Important PDEs, p. 536
- Superposition, p. 536

### Wave equation, p. 538

- Separation of variables, p. 540
- Boundary conditions, p. 541-543
- Fourier series, p. 543-545
- Visualization, p. 546

### D'Alembert's solution, p. 548-550

### Heat equation, diffusion equation with fourier series, p. 552

- Boundary conditions, p. 554
- Fourier series, p. 554-555
- Example (triangular temperature), p. 556

### Laplace's equation, p. 558-560

### Heat equation with fourier transforms, p. 562

- Use of fourier integrals, p. 563-564
- Use of fourier transforms, p. 566-568
-
